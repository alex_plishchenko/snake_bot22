package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.snake.model.Elements;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by Bogdan on 03.12.2015.
 */
public class Deikstra {
    private int INF = Integer.MAX_VALUE / 2;

    private int n;

    private ArrayList<Integer> adj[];
    private ArrayList<Integer> weight[];
    private boolean used[];
    private int dist[];

    private int pred[];
    private char field[][];

    private static int[][] _codeArr;

    private int start;
    private int finish;

    private int finishTemp;
    private  Board bord;

    enum SECTOR { SECTOR_1, SECTOR_2, SECTOR_3, SECTOR_4 }
    private  boolean enableAlg = true;
    private  boolean error = false;
    private  int errorCount = 0;

    private int weightArr[][] = {
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},
    };

    Deikstra(char [][] field, Board b){
        this.field = field;
        this.bord = b;

    }

    private void showAr(int [] ar){
        System.out.print("\nShow Pred");
        for(int i = 1; i<ar.length; i++) {
            if(ar[i-1] == -1 && ar[i] !=-1) { System.out.println("");}
            if(ar[i] == -1) continue;
            System.out.print( ar[i]+ " ");
        }
    }

    private void showTable(int[][] ar){
        System.out.print("\nShow Table\n");
        for(int i = 0; i<ar.length; i++) {
            for (int j = 0; j < ar.length; j++) System.out.print(ar[j][i] + " ");
            System.out.println("");
        }
    }

    private void showBord(char[][] ar){
        System.out.println("\nShow board");
        for(int i = 0; i<ar.length; i++) {
            for (int j = 0; j < ar.length; j++) System.out.print(ar[j][i] + " ");
            System.out.println("");
        }
    }

    private int[][] codeField(char[][] field){

        //showBord(field);

        int [][] codeArr = new int [field.length][field.length];
        int count = 0;
        for (int x = 0; x < field.length; x++) {
            for (int y = 0; y < field.length; y++) {
                char ch = field[x][y];
                if (ch == Elements.TAIL_LEFT_UP.ch() ||
                        ch == Elements.TAIL_END_RIGHT.ch() ||
                        ch == Elements.TAIL_END_LEFT.ch() ||
                        ch == Elements.TAIL_END_DOWN.ch()||
                        ch == Elements.TAIL_VERTICAL.ch()||
                        ch == Elements.TAIL_END_UP.ch() ||
                        ch == Elements.TAIL_HORIZONTAL.ch() ||
                        ch == Elements.TAIL_RIGHT_UP.ch()||
                        ch == Elements.TAIL_LEFT_DOWN.ch()||
                        ch == Elements.TAIL_RIGHT_DOWN.ch()
                        ){
                    codeArr[x][y] = -1;
                    count++;
                    continue;
                }else if(ch == Elements.BREAK.ch()||
                        ch == Elements.BAD_APPLE.ch()){
                    codeArr[x][y] = -1;
                    count++;
                    continue;
                }else if (ch == Elements.HEAD_DOWN.ch()||
                        ch == Elements.HEAD_RIGHT.ch() ||
                        ch == Elements.HEAD_LEFT.ch() ||
                        ch == Elements.HEAD_UP.ch()){
                    codeArr[x][y] = count;
                    start = count++;
                    continue;
                } else if (ch == Elements.GOOD_APPLE.ch()){
                    codeArr[x][y] = count;
                    finish = count++;
                    /*if (flag == 1) finish = 16;
                    if (flag == 2) finish = 28;
                    if (flag == 3) finish = 196;
                    if (flag == 4) finish = 208;
                    */
                } else {codeArr[x][y] = count++;}
                if (ch == Elements.TAIL_VERTICAL.ch()){
                    weightArr[x-1][y] = 0;
                    weightArr[x+1][y] = 0;
                }
                if (ch == Elements.TAIL_HORIZONTAL.ch()){
                    weightArr[x][y-1] = 0;
                    weightArr[x][y+1] = 0;
                }
                if (ch == Elements.TAIL_LEFT_UP.ch() ||
                    ch == Elements.TAIL_END_RIGHT.ch() ||
                    ch == Elements.TAIL_END_LEFT.ch() ||
                    ch == Elements.TAIL_END_DOWN.ch()||
                    ch == Elements.TAIL_END_UP.ch() ||
                    ch == Elements.TAIL_RIGHT_UP.ch()||
                    ch == Elements.TAIL_LEFT_DOWN.ch()||
                    ch == Elements.TAIL_RIGHT_DOWN.ch()){
                    weightArr[x-1][y] = 0;
                    weightArr[x+1][y] = 0;
                    weightArr[x][y-1] = 0;
                    weightArr[x][y+1] = 0;
                }
            }
        }
        return codeArr;
    }

    private void initialData(int [][] codeArr){
        int iter = 0;
        n = codeArr.length * codeArr.length;

        adj = new ArrayList[n];
        for (int i = 0; i < n; i++) {
            adj[i] = new ArrayList<Integer>();
        }

        weight = new ArrayList[n];
        for (int i = 0; i < n; i++) {
            weight[i] = new ArrayList<Integer>();
        }

        for (int i = 0; i < codeArr.length; i++) {
            for (int j = 0; j < codeArr.length; j++) {
                if (codeArr[i][j] == -1 || codeArr[i][j] == -2){
                    iter++; continue;
                } else {
                    if (codeArr[i - 1][j] != -1 && codeArr[i - 1][j] != -2) {
                        if(codeArr[i][j] == -2){
                            weight[iter].add(1);
                        }else weight[iter].add(10);
                        adj[iter].add(codeArr[i - 1][j]);
                    }
                    if (codeArr[i + 1][j] != -1 && codeArr[i + 1][j] != -2) {
                        if(codeArr[i][j] == -2) {
                            weight[iter].add(1);
                        }else weight[iter].add(10);
                        adj[iter].add(codeArr[i + 1][j]);

                    }
                    if (codeArr[i][j - 1] != -1 && codeArr[i][j - 1] != -2) {
                        if(codeArr[i][j] == -2){
                            weight[iter].add(1);
                        }else weight[iter].add(10);
                        adj[iter].add(codeArr[i][j - 1]);
                    }
                    if (codeArr[i][j + 1] != -1 && codeArr[i][j + 1] != -2) {
                        if(codeArr[i][j] == -2){
                            weight[iter].add(1);
                        }else weight[iter].add(10);
                        adj[iter].add(codeArr[i][j + 1]);
                    }
                    iter++;
                    //-------------
                  /*  if (codeArr[i - 1][j] != -2) {
                        adj[iter].add(codeArr[i - 1][j]);
                        weight[iter].add(1);
                    }
                    if (codeArr[i + 1][j] != -2) {
                        adj[iter].add(codeArr[i + 1][j]);
                        weight[iter].add(1);
                    }
                    if (codeArr[i][j - 1] != -2) {
                        adj[iter].add(codeArr[i][j - 1]);
                        weight[iter].add(1);
                    }
                    if (codeArr[i][j + 1] != -2) {
                        adj[iter].add(codeArr[i][j + 1]);
                        weight[iter].add(1);
                    }*/

                   /*if (codeArr[i-1][j] != -1){
                        adj[iter].add(codeArr[i-1][j]);
                        weight[iter].add(weightArr[i-1][j]);
                    } if (codeArr[i+1][j] != -1){
                        adj[iter].add(codeArr[i+1][j]);
                        weight[iter].add(weightArr[i+1][j]);
                    } if (codeArr[i][j-1] != -1){
                        adj[iter].add(codeArr[i][j-1]);
                        weight[iter].add(weightArr[i][j-1]);
                    } if (codeArr[i][j+1] != -1){
                        adj[iter].add(codeArr[i][j+1]);
                        weight[iter].add(weightArr[i][j+1]);
                    } iter++;*/
                }
                }
            }

        used = new boolean[n];
        Arrays.fill(used, false);

        pred = new int[n];
        Arrays.fill(pred, -1);

        dist = new int[n];
        Arrays.fill(dist, INF);
    }

    private int findSpeasSector(SECTOR cur, boolean rand)
    {
        System.out.println("findSpeasSector: " + cur.toString());

        int sumaSpace[] = new int[4];
        int flag2Line[] = new int[4];

        /// sector 4 ->1

        int rerult = 17;

        if(cur == SECTOR.SECTOR_1){
            /*sumaSpace[0] = 0;
            System.out.print("\nShow Sector\n");
            for(int i = 1; i<_codeArr.length/2; i++) {
                for (int j = 1; j < _codeArr.length/2; j++) {
                    System.out.print(_codeArr[j][i] + " ");
                    if(_codeArr[j][i] != -1)  ++sumaSpace[0];
                }
                System.out.println("");
            }*/
        }
        sumaSpace[0] = 0;
        int sec1[] = new int[_codeArr.length/2 + _codeArr.length/2];
        for(int i = 1, index = 0; i<_codeArr.length/2; i++, index++) {
            for (int j = 1; j < _codeArr.length/2; j++) {
                if(_codeArr[j][i] != -1)  {
                    ++sumaSpace[0];
                    sec1[index] = _codeArr[j][i];
                }
            }
        }

        sumaSpace[1] = 0;
        int sec2[] = new int[_codeArr.length/2 + _codeArr.length -1 -7];
        for(int i = 1, index = 0; i<_codeArr.length/2; i++, index++) {
            for (int j = 7; j < _codeArr.length-1; j++) {
                if(_codeArr[j][i] != -1) {
                    ++sumaSpace[1];
                    sec2[index] = _codeArr[j][i];
                }
            }
        }

        sumaSpace[2] = 0;
        int sec3[] = new int[_codeArr.length/2 + _codeArr.length -1 -7];
        for(int i = 7, index = 0; i<_codeArr.length-1; i++, index++) {
            for (int j = 1; j < _codeArr.length/2; j++) {
                if(_codeArr[j][i] != -1)  {
                    ++sumaSpace[2];
                    sec3[index] =_codeArr[j][i];
                }
            }
        }

        sumaSpace[3] = 0;
        int sec4[] = new int[_codeArr.length-1 -7 + _codeArr.length -1 -7];
        for(int i = 7, index = 0; i<_codeArr.length-1; i++, index++) {
            for (int j = 7; j < _codeArr.length-1; j++) {
                if(_codeArr[j][i] != -1) {
                    ++sumaSpace[3];
                    sec4[index] = _codeArr[j][i];
                }
            }
        }

        if(cur == SECTOR.SECTOR_2) {
           /* sumaSpace[1] = 0;
            System.out.print("\nShow Sector\n");
            for(int i = 7; i<_codeArr.length; i++) {
                for (int j = 1; j < _codeArr.length/2; j++) {
                    System.out.print(_codeArr[j][i] + " ");
                    if(_codeArr[j][i] != -1)  ++sumaSpace[1];
                }
                System.out.println("");
            }
            System.out.println(sumaSpace[1]);*/
        }
            /// sector 3 ->2
        if(cur == SECTOR.SECTOR_3){
          /*  System.out.print("\nShow Sector\n");
            for(int i = 1; i<_codeArr.length/3; i++) {
                for (int j = 1; j < _codeArr.length-1; j++) {
                    System.out.print(_codeArr[j][i] + " ");
                    if( (i >= 1 && i <=3) && _codeArr[j][i] != -1)  ++sumaSpace[2];
                }
                System.out.println("");
            }
            System.out.println(sumaSpace[2]);*/
        }
        if(cur == SECTOR.SECTOR_4){
           /* System.out.print("\nShow Sector\n");
            for(int i = 7; i<_codeArr.length; i++) {
                for (int j = 1; j < _codeArr.length-1; j++) {
                    System.out.print(_codeArr[j][i] + " ");
                    if(_codeArr[j][i] != -1)  ++sumaSpace[3];
                }
                System.out.println("");
            }
            System.out.println(sumaSpace[3]);*/
        }

        int max = 0;
        for(int i = 0; i < sumaSpace.length; i++){
            if(sumaSpace[i] > sumaSpace[max]){
                max = i;
            }
        }

        System.out.println(max);

        if(rand == true){
            Random r = new Random();
            int Low = 0;
            int High = 3;
            max = r.nextInt(High-Low) + Low;
        }

        int Low = 0;
        int High = 11;

        if(max == 0){
            Random r = new Random();
            max = r.nextInt(High-Low) + Low;
            rerult = sec1[max];
        }
        if(max == 1){
            Random r = new Random();
            max = r.nextInt(High-Low) + Low;
            rerult = sec2[max];
        }
        if(max == 2){
            /*
            int v = 0;
            for(int i = 7; i<_codeArr.length-1; i++) {
                for (int j = 1; j < _codeArr.length-12; j++) {
                    if(_codeArr[j][i] != -1)  ++v;
                }
            }
            int h = 0;
            for(int i = 11; i<_codeArr.length-2; i++) {
                for (int j = 1; j < _codeArr.length/2; j++) {
                    if(_codeArr[j][i] != -1)  ++h;
                }
            }
            if( v == h) {rerult = 41;}
            else
            {
                if(v > h){
                    rerult = 26;
                }else{
                    rerult = 57;
                }
            }
            */
            Random r = new Random();
            max = r.nextInt(High-Low) + Low;
            rerult = sec3[max];
        }
        if(max == 3){
            Random r = new Random();
            max = r.nextInt(High-Low) + Low;
            rerult = sec4[max];
        }

        /*if(max >=2) {

            int arX[] = new int[12];
            int arY[] = new int[12];
            int len = this.bord.getSnake().size();
            for (int i = 0; i < len; i++) {
                arX[this.bord.getSnake().get(i).getX()] += 1;
                arY[this.bord.getSnake().get(i).getY()] += 1;
            }
            // valid to down;
            int maxXLen = arX[0];
            for (int i = 1; i < arX.length; i++) {
                if (arX[i] > maxXLen) maxXLen = arX[0];
            }
            if (maxXLen >= 11)
                System.out.println();
        }*/

        return  rerult;
    }

    private int getNode(int pos[]){
        int x = pos[0];
        int y = pos[0];
        return  _codeArr[x][y];
    }

    private int[] getXY(int indexNode){
        int posXY[] = new int[2];
        for (int i = 0; i < _codeArr.length; i++) {
            for (int j = 0; j < _codeArr.length; j++) {
                if (_codeArr[i][j] == indexNode) {
                    posXY[0] = i;
                    posXY[1] = j;
                    break;
                } else {/*do nothing;*/}

            }
        }
        return posXY;
    }

    public void dejkstra(int s) {
        dist[s] = 0;
        for (int iter = 0; iter < n; iter++) {
            if (adj[iter] == null){continue;}
            int v = -1;
            int distV = INF;

            for (int i = 0; i < n; ++i) {
                if (adj[i] == null){ continue; }
                if (used[i]) { continue; }
                if (distV < dist[i]) { continue; }
                v = i;
                distV = dist[i];
            }

            for (int i = 0; i < adj[v].size(); i++) {
                int u = adj[v].get(i);
                int weightU = weight[v].get(i);

                if (dist[v] + weightU < dist[u]) {
                    dist[u] = dist[v] + weightU;
                    pred[u] = v;
                }
            }
            used[v] = true;
        }
    }

    public boolean error(){
        return  error;
    }

    public int[] nextStep(){

        int [] pos = new int[2];

        if(bord.getSnake().size() > 5){
            bord.getSnakeDirection();
        }

        for(int i = 0; i< 2; )
        {
            pos = nextStepDejkstra();
            if (start == finishTemp) enableAlg = true;
            if(errorCount >6) { errorCount = 0; return pos;}

            if (error == false) {enableAlg = true; return pos; }

            errorCount++;

            pos = getXY(start);

            SECTOR curSector = SECTOR.SECTOR_1;
            /// #1
            if (pos[0] < 7 && pos[1] < 7) {
                curSector = SECTOR.SECTOR_1;
            }
            // #2
            if (pos[0] >= 7 && pos[1] < 7) {
                curSector = SECTOR.SECTOR_2;
            }
            /// #3
            if (pos[0] < 7 && pos[1] >= 7) {
                curSector = SECTOR.SECTOR_3;
            }

            // #4
            if (pos[0] >= 7 && pos[1] >= 7) {
                curSector = SECTOR.SECTOR_4;
            }

            finish = findSpeasSector(curSector, false);
            finishTemp = finish;
            enableAlg = false;

            pos = nextStepDejkstra();
            if (error == true){
                finish = findSpeasSector(curSector, true);
                finishTemp = finish;
                enableAlg = false;
                errorCount++;
                pos = nextStepDejkstra();
            }else  enableAlg = true;

            //System.out.println("Star: " + pos[0] + ":" + pos[1]);
        }

        return pos;
    }

    public int[] nextStepDejkstra(int stat, int finish){
        int [] buf = new int[2];
        return  buf;
    }
    public int[] nextStepDejkstra(){
        int [] buf = new int[2];
        int v = 0, bufv = 0;
        int[][] codeArr = codeField(field);
        _codeArr = codeArr;

        //showTable(codeArr);

        initialData(codeArr);

        dejkstra(start);

        v = finish;
       // System.out.println("\n-----------------------");

        while (v != start ){
            bufv = v;
            v = pred[v];
            if (v == -1) break;
        }

      //  System.out.println("Finish: " + finish + "  start: " + start);
      //  System.out.println("\n-----------------------");

        buf = getXY(bufv);

       // System.out.println("node: " + v +" -> " + bufv + " pos  " +  buf[0] + ":" + buf[1] + " start: " + start);
      //  System.out.println("Len : " + bord.getSnake().size());

        if( v == -1){
            error = true;
        }else{
            error = false;
        }

       return  buf;
    }
}
