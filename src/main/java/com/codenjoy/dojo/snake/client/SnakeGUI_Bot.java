package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.snake.model.Elements;

import javax.swing.*;
import java.awt.*;

import static com.codenjoy.dojo.snake.model.Elements.*;

/**
 * Created by Plishchenko1 on 08.12.2015.
 */
public class SnakeGUI_Bot {
    JFrame frame;
    DrawPanel drawPanel;

    private int oneX = 7;
    private int oneY = 7;
    private int _W = 50;
    private int _H = 50;

    SnakeSS _snake;

    private int _timeSleep = 250;

    public void set(SnakeSS snake){
        _snake = snake;
    }

    public void go(int tSleep) throws InterruptedException {
        _timeSleep = tSleep;
        go();
    }

    public void go() throws InterruptedException {

        frame = new JFrame("Snake Bot2");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        drawPanel = new DrawPanel();

        frame.getContentPane().add(BorderLayout.CENTER, drawPanel);

        frame.setVisible(true);
        frame.setResizable(false);
        frame.setSize(_H*15 + 100 , _H*15 + 100);
        frame.setLocation(375, 55);

        moveIt();
    }
    class DrawPanel extends JPanel {
        public void paintComponent(Graphics g) {

            g.setColor(Color.WHITE);
            g.fillRect(1, 1, this.getWidth()-2, this.getHeight()-2);

            int indexColor = 0;

            int sizeH= _snake.getField().length;
            char [][] ch = _snake.getField();
            int dx = 0;
            int dy = 0;
            char el;
            for (int i = 0; i < sizeH; i++){
                for (int j = 0; j < sizeH; j++){

                    el = ch[j][i];

                    if(el == BREAK.ch()){
                        g.setColor(Color.BLACK);
                    }else
                        if(el == NONE.ch()){
                        g.setColor(Color.WHITE);
                    }else
                    if(el == GOOD_APPLE.ch()){
                        g.setColor(Color.GREEN);
                    }else
                    if(el == BAD_APPLE.ch()){
                        g.setColor(Color.RED);
                    }else {
                        if(el == Elements.HEAD_DOWN.ch()
                                ||el == Elements.HEAD_UP.ch()
                                ||el == Elements.HEAD_LEFT.ch()
                                ||el == Elements.HEAD_RIGHT.ch()
                                ) {
                            g.setColor(Color.YELLOW);
                        }else{
                            indexColor += 5;
                            if( indexColor > 255) indexColor = 0;
                            g.setColor( new Color(indexColor, 0 ,255) ); // 0, 0, 255 =  blue
                        }
                    }
                    g.fillRect(dx, dy, _W, _H);
                    dx += _W;
                }
                dx = 0;
                dy += _H;
            }
        }
    }
    private void moveIt() throws InterruptedException {
        while(true){

            _snake.run();

            try{
                Thread.sleep(_timeSleep);
            } catch (Exception exc){}
            frame.repaint();
        }
    }
}
