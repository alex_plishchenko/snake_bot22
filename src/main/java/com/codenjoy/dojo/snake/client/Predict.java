package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.services.RandomDice;

import java.util.Arrays;


public class Predict {
    public Predict(BoardFix board, YourSolver solver){
        _solver = solver;
        _board = board;
    }

    public boolean isPathForApple(int nAplle) throws InterruptedException {

        BoardFix boardTemp  = new BoardFix();

        for (int i = 0; i<nAplle; i++){

            boardTemp.forString(Arrays.deepToString(_board.getField()));

            SnakeSS _snake = new SnakeSS(boardTemp, _solver);

            if(!_snake.runNoSleep()) {
                return false;
            }
        }
        return true;
    }

    private BoardFix _board;
    private YourSolver _solver;
}
