package com.codenjoy.dojo.snake.client;


import com.codenjoy.dojo.client.Direction;
import com.codenjoy.dojo.client.Solver;

import com.codenjoy.dojo.services.Dice;

import com.codenjoy.dojo.services.RandomDice;
import com.codenjoy.dojo.snake.model.Elements;

/**
 * User: your name
 */
public class YourSolver implements Solver<Board> {

    private static final String USER_NAME = "faranheit0@gmail.com";

    private Dice dice;
    private Board board;

    public YourSolver(Dice dice) {
        this.dice = dice;
    }

    public static char[][] forS(String boardString) {
        String board = boardString.replaceAll("\n", "");
        int size = (int) Math.sqrt(board.length());

        char[] temp = board.toCharArray();
        char[][] field = new char[size][size];
        for (int y = 0; y < size; y++) {
            int dy = y*size;
            for (int x = 0; x < size; x++) {
                field[x][y] = temp[dy + x];
            }
        }
        return field;
    }

    @Override
    public String get(Board board) {
        this.board = board;

        char[][] field = board.getField();

        // found snake
        int snakeHeadX = -1;
        int snakeHeadY = -1;
        for (int x = 0; x < field.length; x++) {
            for (int y = 0; y < field.length; y++) {
                char ch = field[x][y];
                if (ch == Elements.HEAD_DOWN.ch() ||
                        ch == Elements.HEAD_UP.ch() ||
                        ch == Elements.HEAD_LEFT.ch() ||
                        ch == Elements.HEAD_RIGHT.ch()) {
                    snakeHeadX = x;
                    snakeHeadY = y;
                    break;
                }
            }
        }

        Deikstra deikstra = new Deikstra(field, board);
        int []apple = deikstra.nextStep();

        int dx = snakeHeadX - apple[0];
        int dy = snakeHeadY - apple[1];

        if (dx < 0) {
            return Direction.RIGHT.toString();
        }
        if (dx > 0) {
            return Direction.LEFT.toString();
        }
        if (dy < 0) {
            return Direction.DOWN.toString();
        }
        if (dy > 0) {
            return Direction.UP.toString();
        }

        return Direction.UP.toString();
    }

    public static void main(String[] args) {
        start(USER_NAME, MyWebSocketRunner.Host.REMOTE);
    }

    public static void start(String name, MyWebSocketRunner.Host server) {
        try {
            String boardString =
                            "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼" +
                            "☼             ☼" +
                            "☼             ☼" +
                            "☼             ☼" +
                            "☼             ☼" +
                            "☼             ☼" +
                            "☼             ☼" +
                            "☼             ☼" +
                            "☼             ☼" +
                            "☼             ☼" +
                            "☼          ☻  ☼" +
                            "☼       ◄═    ☼" +
                            "☼             ☼" +
                            "☼     ☺       ☼" +
                            "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼";


            BoardFix board = new BoardFix();
            board.forString(boardString);

            SnakeSS snake = new SnakeSS( board , new YourSolver(new RandomDice()));

            SnakeGUI_Bot gui = new SnakeGUI_Bot();
            gui.set(snake);
            gui.go(100);

           // MyWebSocketRunner.run(server, name, new YourSolver(new RandomDice()), new Board());


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
