package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.client.AbstractBoard;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.snake.model.*;
import com.codenjoy.dojo.snake.model.Elements;

import java.util.List;

/**
 * Created by Plishchenko1 on 08.12.2015.
 */
public class BoardFix extends Board {
    @Override
    public Point getHead() {
        List<Point> result = get(
                Elements.HEAD_UP,
                Elements.HEAD_DOWN,
                Elements.HEAD_LEFT,
                Elements.HEAD_RIGHT);
        if(result.size() == 0){
            System.exit(-1);
        }
        return result.get(0);
    }
}
