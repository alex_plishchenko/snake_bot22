package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.client.Direction;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.snake.model.Elements;

import java.util.LinkedList;
import java.util.Random;



/**
 * Created by alexsolt on 07.12.15.
 */
public class SnakeSS {

    String _boardDefault =
                    "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼" +
                    "☼             ☼" +
                    "☼             ☼" +
                    "☼             ☼" +
                    "☼             ☼" +
                    "☼             ☼" +
                    "☼   ═►        ☼" +
                    "☼             ☼" +
                    "☼             ☼" +
                    "☼             ☼" +
                    "☼          ☻  ☼" +
                    "☼             ☼" +
                    "☼             ☼" +
                    "☼     ☺       ☼" +
                    "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼";

    private BoardFix _border;
    private char _field[][];
    private  YourSolver _solver;
    private int _appleCount = 0;

    private int _snakeLenStart;

    private Point _pHead;

    class snakeCell{
        snakeCell(){};
        snakeCell(char c, int[] p){
            _ch = c;
            _pos = p;
        }

        @Override
        public String toString(){
            return String.format("[" + _pos[0] + "," + _pos[1] + "]");
        }

        public char _ch;
        public int[] _pos;
        public int getX() { return _pos[0];}
        public int getY() { return _pos[1];}
    }

    private  LinkedList<snakeCell> _snakeBody = new LinkedList<>();

    public char[][] getField()
    {
        return  _field;
    }

    public SnakeSS(YourSolver solver){

        _border = new BoardFix();
        _border.forString(_boardDefault);
        initData(_border, solver);
    }

    public SnakeSS(BoardFix board, YourSolver solver){
        initData(board, solver);
    }

    public void run() throws InterruptedException {
            String answer  = _solver.get(_border);
            upDate(answer);
    }

    public boolean runNoSleep() throws InterruptedException {

        boolean result = true;
        while (true) {
            String answer  = _solver.get(_border);
            if(!upDate(answer)) {result  = false; break;}
        }
        return result;
    }

    private void initData(BoardFix board, YourSolver solver){
        _border = board;
        _field = _border.getField();
        _solver = solver;

        for(Point i: _border.getSnake()){
            snakeCell elSnake = new snakeCell();
            _snakeBody.add(new snakeCell(_field[i.getX()][i.getY()], new int[]{i.getX(), i.getY()}));
        }
        _snakeLenStart = _snakeBody.size();
    }

    private void upDate(){
        snakeDraw();
        randomAllpe();
    }

    private void msgGameOver(){
        System.out.print( "|=====================|\n");
        System.out.print( "|      Game Over      |\n");
        System.out.print( "|=====================|\n");
    }

    private void  logSnow(){
        System.out.println(_border.toString());

        System.out.println( "\nApple suma: " +  _appleCount);
        System.out.println( "Snake: ");
        System.out.println( "    len start: " +  _snakeLenStart);
        System.out.println( "    cur start: " );
        System.out.println( "        border   : " + _border.getSnake().size());
        System.out.println( "        snakeBody: " + _snakeBody.size());
        System.out.println( "    body: " + _snakeBody.toString());
    }

    private boolean upDate(String answer){

        _pHead = _border.getHead();

        try {
            if(Direction.RIGHT.toString() == answer){
                moveRight();
            }else if(Direction.LEFT.toString() == answer){
                moveLeft();
            }else  if(Direction.UP.toString() == answer) {
                moveUp();
            }else  if(Direction.DOWN.toString() == answer)
                moveDown();

            snakeDraw();
            randomAllpe();

            return true;

       }catch (IndexOutOfBoundsException ex) {
            System.exit(-1);
            return false;
        }
    }

    private void snakeDraw()
    {
        for(snakeCell i: _snakeBody){
            char c = i._ch;
            _field[i.getX()][i.getY()] = c;
        }
    }

    private void randomAllpe(){
        if(!_border.getApples().isEmpty()) {  return; }
        int x = 0;
        int y = 0;

        while (true)
        {
            x = new Random().nextInt(10) + 2;
            y = new Random().nextInt(10) + 2;
            if(_field[x][y] == ' '){
                _field[x][y] = Elements.GOOD_APPLE.ch();
                ++_appleCount;
                return;
            }
        }
    }

    private  boolean isGrow(Direction cutDir){
        Point pHead = _border.getHead();
        if(cutDir == Direction.LEFT){
            if(_field[pHead.getX() - 1][pHead.getY()] == Elements.GOOD_APPLE.ch()){
                return true;
            }
        }
        if(cutDir == Direction.RIGHT){
            if(_field[pHead.getX() + 1][pHead.getY()] == Elements.GOOD_APPLE.ch()){
                return true;
            }
        }
        if (cutDir == Direction.DOWN){
            if(_field[pHead.getX()][pHead.getY() + 1] == Elements.GOOD_APPLE.ch()){
                return true;
            }
        }
        if (cutDir == Direction.UP){
            if(_field[pHead.getX()][pHead.getY() - 1 ] == Elements.GOOD_APPLE.ch()){
                return true;
            }
        }
        _field[_snakeBody.getLast().getX()][_snakeBody.getLast().getY()] = Elements.NONE.ch();
        _snakeBody.removeLast();
        return false;
    }

    private void moveDown() {
        if (_border.getSnakeDirection() == Direction.DOWN) { stepNext(); return;}

        Direction cur = _border.getSnakeDirection();

        isGrow(Direction.DOWN);

        if(cur == Direction.RIGHT) {
            _snakeBody.set(0, new snakeCell(Elements.TAIL_LEFT_DOWN.ch(), new int[]{_pHead.getX(), _pHead.getY()}));
        }
        else {
            _snakeBody.set(0, new snakeCell(Elements.TAIL_RIGHT_DOWN.ch(), new int[]{_pHead.getX(), _pHead.getY()}));
        }
        _snakeBody.add(0,new snakeCell(Elements.HEAD_DOWN.ch(), new int[]{_pHead.getX(), _pHead.getY() + 1}));

        upDate();
    }

    private void moveUp() {
        if(_border.getSnakeDirection() == Direction.UP) { stepNext(); return; }

        Direction cur = _border.getSnakeDirection();

        isGrow(Direction.UP);

        if(cur == Direction.RIGHT) {
            _snakeBody.set(0, new snakeCell(Elements.TAIL_LEFT_UP.ch(), new int[]{_pHead.getX(), _pHead.getY()}));
        }
        else {
            _snakeBody.set(0, new snakeCell(Elements.TAIL_RIGHT_UP.ch(), new int[]{_pHead.getX(), _pHead.getY()}));
        }
        _snakeBody.add(0,new snakeCell(Elements.HEAD_UP.ch(), new int[]{_pHead.getX(), _pHead.getY() - 1}));
        upDate();
    }

    private void moveLeft() {
        if (_border.getSnakeDirection() == Direction.LEFT) { stepNext(); return;}

        Direction cur = _border.getSnakeDirection();

        isGrow(Direction.LEFT);

        if(cur == Direction.UP) {
            _snakeBody.set(0, new snakeCell(Elements.TAIL_LEFT_DOWN.ch(), new int[]{_pHead.getX(), _pHead.getY()}));
        }
        else {
            _snakeBody.set(0, new snakeCell(Elements.TAIL_LEFT_UP.ch(), new int[]{_pHead.getX(), _pHead.getY()}));
        }
        _snakeBody.add(0,new snakeCell(Elements.HEAD_LEFT.ch(), new int[]{_pHead.getX() - 1, _pHead.getY()}));
        upDate();
    }

    private void moveRight() {
        if(_border.getSnakeDirection() == Direction.RIGHT) { stepNext(); return;}


        Direction cur = _border.getSnakeDirection();

        isGrow(Direction.RIGHT);

        if(cur == Direction.UP) {
            _snakeBody.set(0, new snakeCell(Elements.TAIL_RIGHT_DOWN.ch(), new int[]{_pHead.getX(), _pHead.getY()}));
        }
        else {
            _snakeBody.set(0, new snakeCell(Elements.TAIL_RIGHT_UP.ch(), new int[]{_pHead.getX(), _pHead.getY()}));
        }
        _snakeBody.add(0,new snakeCell(Elements.HEAD_RIGHT.ch(), new int[]{_pHead.getX() + 1, _pHead.getY()}));

        upDate();
    }

    private boolean stepNext(int nStep) {
        boolean res = false;
        for(int i = 0; i < nStep; i++){
            res = stepNext();
            if(stepNext()) {return res;};
        }
        return res;
    }

    private boolean stepNext() {
        Point pHead = _border.getHead();

        Direction cur = _border.getSnakeDirection();

        isGrow(cur);

        if(cur == Direction.RIGHT  ||  cur == Direction.LEFT) {
                _snakeBody.set(0, new snakeCell(Elements.TAIL_HORIZONTAL.ch(), new int[]{pHead.getX(), pHead.getY()}));
        }
        else{
            _snakeBody.set(0, new snakeCell(Elements.TAIL_VERTICAL.ch(), new int[]{pHead.getX(), pHead.getY()}));
        }

        if(cur == Direction.RIGHT || cur == Direction.LEFT){
            if(cur == Direction.RIGHT){
                _snakeBody.add(0,new snakeCell(Elements.HEAD_RIGHT.ch(), new int[]{pHead.getX()+1, pHead.getY()}));
            }
            else {
                _snakeBody.add(0,new snakeCell(Elements.HEAD_LEFT.ch(), new int[]{pHead.getX() - 1, pHead.getY()}));
            }
        }
        else {
            if(cur == Direction.DOWN){
                _snakeBody.add(0,new snakeCell(Elements.HEAD_DOWN.ch(), new int[]{pHead.getX(), pHead.getY()+1}));
            }
            else {
                _snakeBody.add(0,new snakeCell(Elements.HEAD_UP.ch(), new int[]{pHead.getX(), pHead.getY()-1}));
            }
        }

        upDate();

        return false;
    }

}
